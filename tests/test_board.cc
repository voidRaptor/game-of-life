#include <board/cell.hh>
#include <board/board.hh>
#include <catch2/catch_test_macros.hpp>

class BoardTester
{
public:
    unsigned int getSize()
    {
        return sut.mCells.size();
    }

    Board sut;
};


TEST_CASE_METHOD(BoardTester, "board is empty when width and height are 0", "[createBoard]")
{
    sut.createBoard(0, 0);
    REQUIRE(getSize() == 0);
}

TEST_CASE_METHOD(BoardTester, "board filled when using non-zero width and height", "[createBoard]")
{
    sut.createBoard(4, 4);
    REQUIRE(getSize() == 16);

    for (unsigned int j=0; j<4; ++j)
    {
        for (unsigned int i=0; i<4; ++i)
        {
            auto cell = sut.getCell(i, j);
            REQUIRE(cell.x == i);
            REQUIRE(cell.y == j);
            REQUIRE(cell.state == State::Dead);
        }
    }
}

auto basicSetup()
{
/*
   D = dead
   A = alive

   D D D D
   D A A A
   D A A A
   D A A A
*/
    std::vector<Cell> cells;

    for (unsigned int j=0; j<4; ++j)
    {
        for (unsigned int i=0; i<4; ++i)
            cells.emplace_back(i, j, State::Alive);
    }

    cells[0].state = State::Dead;
    cells[1].state = State::Dead;
    cells[2].state = State::Dead;
    cells[3].state = State::Dead;

    cells[4].state = State::Dead;
    cells[8].state = State::Dead;
    cells[12].state = State::Dead;

    // j * rowwidth + i
    REQUIRE(cells[0 * 4 + 0].state == State::Dead);
    REQUIRE(cells[1 * 4 + 1].state == State::Alive);
    REQUIRE(cells[2 * 4 + 2].state == State::Alive);
    REQUIRE(cells[3 * 4 + 3].state == State::Alive);

    return cells;
}

TEST_CASE("test countNeighbours", "[countNeighbours]")
{
    auto cells = basicSetup();
    Board sut;
    sut.setCells(cells, 4, 4);

    SECTION("neighbours are counted correctly")
    {
        REQUIRE(sut.countNeighbours(2, 2) == 8);
        REQUIRE(sut.countNeighbours(0, 0) == 1);
    }

    SECTION("neighbours have bounds")
    {
        REQUIRE(sut.countNeighbours(-1, -1) == 0); // TODO: check if throws
    }
}


TEST_CASE("toggling operates on correct cell", "[toggleCellState]")
{
    auto cells = basicSetup();
    Board sut;
    sut.setCells(cells, 4, 4);

    auto cell = sut.getCell(0, 0);
    REQUIRE(cell.x == 0);
    REQUIRE(cell.y == 0);

    auto toggled = sut.toggleCellState(0, 0);
    REQUIRE(toggled.x == 0);
    REQUIRE(toggled.y == 0);
}

TEST_CASE("cell state changes from dead to alive when toggled", "[toggleCellState]")
{
    auto cells = basicSetup();
    Board sut;
    sut.setCells(cells, 4, 4);

    auto cell = sut.getCell(0, 0);
    REQUIRE(cell.state == State::Dead);
    REQUIRE(cell.x == 0);
    REQUIRE(cell.y == 0);

    auto toggled = sut.toggleCellState(0, 0);
    REQUIRE(toggled.state == State::Alive);
    REQUIRE(toggled.x == 0);
    REQUIRE(toggled.y == 0);
}

TEST_CASE("cell state changes from alive to when toggled", "[toggleCellState]")
{
    auto cells = basicSetup();
    Board sut;
    sut.setCells(cells, 4, 4);

    auto cell = sut.getCell(3, 3);
    REQUIRE(cell.state == State::Alive);

    auto toggled = sut.toggleCellState(3, 3);
    REQUIRE(toggled.state == State::Dead);
}

/*
TEST_CASE("toggling has bounds", "[toggleCellState][!throws]")
{
    (sut.toggleCellState(-1, -1)); // TODO: check if throws
}
*/

auto unchangingSetup()
{
/*
    D = dead
    A = alive

    D D D D
    D A A D
    D A A D
    D D D D
*/
    std::vector<Cell> cells;

    for (unsigned int j=0; j<4; ++j)
    {
        for (unsigned int i=0; i<4; ++i)
            cells.emplace_back(i, j, State::Dead);
    }
    cells[1 * 4 + 1].state = State::Alive;
    cells[2 * 4 + 1].state = State::Alive;
    cells[1 * 4 + 2].state = State::Alive;
    cells[2 * 4 + 2].state = State::Alive;

    return cells;
}

TEST_CASE("next generation is unchanged for square shape", "[getNextGeneration]")
{
    Board sut;
    auto cells = unchangingSetup();
    sut.setCells(cells, 4, 4);

    REQUIRE(sut.getCell(1, 1).state == State::Alive);
    REQUIRE(sut.getCell(2, 1).state == State::Alive);
    REQUIRE(sut.getCell(1, 2).state == State::Alive);
    REQUIRE(sut.getCell(2, 2).state == State::Alive);

    auto next = sut.getNextGeneration();

    for (unsigned int i=0; i<next.size(); ++i)
    {
        REQUIRE(next[i].state == cells[i].state);
        REQUIRE(next[i].x == cells[i].x);
        REQUIRE(next[i].y == cells[i].y);
    }
}

TEST_CASE_METHOD(BoardTester, "getNextGeneration doesn't alter internal state", "[getNextGeneration]")
{
    auto cells = basicSetup();
    Board sut;
    sut.setCells(cells, 4, 4);
    sut.getNextGeneration();

    for (unsigned int j=0; j<4; ++j)
    {
        for (unsigned int i=0; i<4; ++i)
        {
            REQUIRE(cells[j * 4 + i].state == sut.getCell(i, j).state);
            REQUIRE(cells[j * 4 + i].x     == sut.getCell(i, j).x);
            REQUIRE(cells[j * 4 + i].y     == sut.getCell(i, j).y);
        }
    }
}

