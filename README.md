# Game Of Life

*NOTE*: tested with Fedora linux + i3, other platforms may not work.

## Dependencies
- Qt6 
- Catch2
- Spdlog
- CMake (>= 3.22)

## Project structure
```
.
├── CMakeLists.txt
├── README.md
├── lib
│   └── catch2
├── src
│   ├── board
│   ├── graphics
│   └── utility
└── tests
```

## How to build

### Submodules
```
git submodule init
git submodule update
```

According to Catch2
[documentation](https://github.com/catchorg/Catch2/blob/devel/docs/cmake-integration.md#installing-catch2-from-git-repository),
if Catch2 3.x is not available via packet managers:
```
cd lib/catch2
cmake -B build -S . -DBUILD_TESTING=OFF
sudo cmake --build build/ -j --target install
```

### Game

In project root:
```
cmake -B build
cmake --build build -t game -j
```


### Unit tests

In project root:
```
cmake -B build
cmake --build build -t tests -j
```


### All at once

In project root:
```
cmake -B build
cmake --build build -j
```


## How to run

In project root, run either
```
./build/bin/game
```
or
```
./build/bin/tests
```

## Gameplay

First, board size must be chosen. After that, board with dead cells is
generated. Cells can be freely marked as *ALIVE* by clicking them with
*MOUSE1*. Next iteration of the game can be inited with *SPACE*.

