#ifndef GAME_HH
#define GAME_HH

#include <board/cell.hh>
#include <board/board.hh>

#include <graphics/mainwindow.hh>
#include <graphics/setupdialog.hh>

#include <QGraphicsRectItem>
#include <QObject>
#include <QTimer>

class Board;

/**
 * @brief Top level handler of the game.
 */
class Game: public QObject
{
    Q_OBJECT
public:
    Game();
    ~Game();
    bool runBoardDialog();
    void run();
    bool createBoard();
    void updateCells();

public slots:
    void toggleCellState(unsigned int x, unsigned int y);
    void setBoardSize(unsigned int width, unsigned int height);

private:
    MainWindow mWindow;
    SetupDialog mDialog;

    QTimer* mFrameTimer;

    unsigned int mCellWidth;
    unsigned int mCellHeight;

    unsigned int mRowSize;
    unsigned int mColSize;

    Board mBoard;
};


#endif
