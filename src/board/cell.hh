#ifndef CELL_HH
#define CELL_HH

enum State {Alive, Dead};

class Cell
{
public:
    Cell(unsigned int x, unsigned int y, State s):
        x(x),
        y(y),
        state(s)
    {}

    Cell():
        x(0),
        y(0),
        state(State::Dead)
    {}

    unsigned int x;
    unsigned int y;

    State state;
};



#endif
