#ifndef BOARD_HH
#define BOARD_HH

#include <vector>

class Cell;

/**
 * @brief Represents grid of cells for the game. Contains only data and no visuals.
 *
 */
class Board
{
    friend class BoardTester;
public:
    Board();

    /**
     * @brief Create new board with all cells dead.
     *
     * @param rowSize, size of the new grid row
     * @param colSize, size of the new grid column
     */
    void createBoard(unsigned int rowSize, unsigned int colSize);

    Cell toggleCellState(unsigned int i, unsigned int j);

    /**
     * @brief Retrieve copy of cell. T
     *
     * @param i, index in the grid row
     * @param j, index in the grid column
     *
     * @throws according to std::vector::at()
     * @return copy of cell
     */
    Cell getCell(unsigned int i, unsigned int j);

    /**
     * @brief Generates next generation of game of life without altering state.
     *
     * @return new generation as vector of cells
     */
    std::vector<Cell> getNextGeneration();

    /**
     * @brief Sets given cells as the new grid. Row and Column sizes must be
     * given to assume correct shape.
     *
     * @param cells, new cells
     * @param rowSize, size of the new grid row
     * @param colSize, size of the new grid column
     */
    void setCells(std::vector<Cell>& cells, unsigned int rowSize, unsigned int colSize);

    unsigned int countNeighbours(unsigned int i, unsigned int j);

private:
    std::vector<Cell> mCells;
    unsigned int mRowSize;
    unsigned int mColSize;

};

#endif
