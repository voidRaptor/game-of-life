#include "board.hh"
#include "cell.hh"
#include <utility/logging.hh>


Board::Board():
    mRowSize(0),
    mColSize(0)
{}


Cell Board::getCell(unsigned int i, unsigned int j)
{
    return mCells.at(j * mRowSize + i);
}

void Board::setCells(std::vector<Cell>& cells, unsigned int rowSize, unsigned int colSize)
{
    mCells = cells;
    mRowSize = rowSize;
    mColSize = colSize;
}

unsigned int Board::countNeighbours(unsigned int cellX, unsigned int cellY)
{
    unsigned int neighbours = 0;

    if (cellX >= mRowSize | cellY >= mColSize)
        return 0;

    for (int y=-1; y<2; ++y)
    {
        for (int x=-1; x<2; ++x)
        {
            // cell itself
            if (x == 0 && y == 0)
                continue;

            int neighbourX = cellX + x;
            int neighbourY = cellY + y;

            // top and left edge of the board
            if (neighbourX < 0 || neighbourY < 0)
                continue;

            // bottom and right edge of the board
            if (neighbourX >= mRowSize || neighbourY >= mColSize)
                continue;

            assert((neighbourY * mRowSize + neighbourX) < mCells.size());

            if (mCells[neighbourY * mRowSize + neighbourX].state == State::Alive)
                ++neighbours;
        }

    }

    return neighbours;
}


std::vector<Cell> Board::getNextGeneration()
{
    std::vector<Cell> nextGenCells;
    nextGenCells.reserve(mRowSize * mColSize);

    for (unsigned int y=0; y<mColSize; ++y)
    {
        for (unsigned int x=0; x<mColSize; ++x)
            nextGenCells.emplace_back(x, y, State::Dead);
    }

    for (unsigned int i=0; i<mCells.size(); ++i)
    {
        unsigned int neighbours = countNeighbours(mCells[i].x, mCells[i].y);

        // Board of life rules
        if (mCells[i].state == State::Alive)
        {
            if (neighbours <= 1 || neighbours >= 4)
                nextGenCells[i].state = State::Dead;
            else
                nextGenCells[i].state = State::Alive;
        }
        else
        {
            if (neighbours == 3)
                nextGenCells[i].state = State::Alive;
        }
    }

    return nextGenCells;
}


void Board::createBoard(unsigned int rowSize, unsigned int colSize)
{
    mCells.clear();

    mRowSize = rowSize;
    mColSize = colSize;
    mCells.reserve(mRowSize * mColSize);

    for (unsigned int y=0; y<mColSize; ++y)
    {
        for (unsigned int x=0; x<mColSize; ++x)
            mCells.emplace_back(x, y, State::Dead);
    }
}


Cell Board::toggleCellState(unsigned int i, unsigned int j)
{
    auto index = j * mRowSize + i;
    assert(index < mCells.size());

    auto previousState = static_cast<unsigned>(mCells[index].state);
    assert(previousState == 0 || previousState == 1); // in case new State is added
    mCells[index].state = static_cast<State>(1 - previousState);

    return mCells[index];
}
