#include "game.hh"
#include <utility/logging.hh>
#include <graphics/graphicsscene.hh>

#include <QGraphicsView>
#include <cassert>

static const unsigned int WINDOW_WIDTH  = 800;
static const unsigned int WINDOW_HEIGHT = 800;
static const unsigned int PADDING = 20;

Game::Game():
    mFrameTimer(new QTimer(this)),
    mCellWidth(0),
    mCellHeight(0),
    mRowSize(0),
    mColSize(0)  // TODO: non square board causes qt coordinate transformation errors
{
    auto* scene = static_cast<QGraphicsView*> (mWindow.centralWidget())->scene();

    QObject::connect(
        static_cast<GraphicsScene*>(scene),
        &GraphicsScene::cellClicked,
        this,
        &Game::toggleCellState
    );

    QObject::connect(mFrameTimer, &QTimer::timeout, this, &Game::updateCells);
    QObject::connect(&mDialog, &SetupDialog::boardSizeChosen, this, &Game::setBoardSize);
    QObject::connect(&mWindow, &MainWindow::nextStepRequested, this, &Game::updateCells);
}


Game::~Game()
{}


bool Game::runBoardDialog()
{
    if (mDialog.exec() == QDialog::Rejected)
    {
        LOG_INFO("Dialog closed without setting values.")
        return false;
    }

    return true;
}


void Game::setBoardSize(unsigned int width, unsigned int height)
{
    mRowSize = width;
    mColSize = height;

    mCellWidth = (WINDOW_WIDTH  - PADDING) / mRowSize;
    mCellHeight = (WINDOW_HEIGHT - PADDING) / mColSize;

    LOG_INFO("Board size: {}x{}", width, height);
    LOG_INFO("Cell size {}x{}", mCellWidth, mCellHeight);
}

void Game::run()
{
    mWindow.setMinimumSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    mWindow.show();
    //mFrameTimer->start(2000); // TODO: only when start is pressed
}

void Game::updateCells()
{
    auto cells = mBoard.getNextGeneration();
    mBoard.setCells(cells, mRowSize, mColSize);

    auto* view = static_cast<QGraphicsView*>(mWindow.centralWidget());
    auto* scene = static_cast<GraphicsScene*>(view->scene());

    for (unsigned int j=0; j<mColSize; ++j)
    {
        for (unsigned int i=0; i<mRowSize; ++i)
            scene->paintByState(mBoard.getCell(i, j), mCellWidth, mCellHeight);
    }
}

bool Game::createBoard()
{
    LOG_INFO("Initializing game...");

    mBoard.createBoard(mRowSize, mColSize);
    auto* view = static_cast<QGraphicsView*> (mWindow.centralWidget());
    auto* scene = static_cast<GraphicsScene*>(view->scene());

    for (unsigned int j=0; j<mColSize; ++j)
    {
        for (unsigned int i=0; i<mRowSize; ++i)
        {
            auto* item = new QGraphicsRectItem(i * mCellWidth, j * mCellHeight, mCellWidth, mCellHeight);
            scene->addItem(item);
            LOG_DEBUG("i: {}, j: {}", i, j);
            scene->paintByState(mBoard.getCell(i, j), mCellWidth, mCellHeight);
        }
    }
    scene->setSceneRect(scene->itemsBoundingRect());

    LOG_INFO("Game initialized.");
    return true;
}

void Game::toggleCellState(unsigned int x, unsigned int y)
{
    auto i = x / mCellWidth;
    auto j = y / mCellHeight;
    LOG_DEBUG("cell {},{} px – {},{}", x, y, i, j);

    auto cell = mBoard.toggleCellState(i, j);
    mWindow.getScene()->paintByState(cell, mCellWidth, mCellHeight);
}

