#include "game.hh"
#include <utility/logging.hh>
#include <QApplication>

int main(int argc, char** argv)
{
    Logging::init();
    QApplication app(argc, argv);
    Game gm;

    int success = 0;
    if (gm.runBoardDialog())
    {
        gm.createBoard();
        gm.run();
        success = app.exec();
    }

    LOG_INFO("Shutdown");
    Logging::destroy();
    return success;
}

