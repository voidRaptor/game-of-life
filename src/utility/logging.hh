#ifndef LOGGING_HH
#define LOGGING_HH

#include <spdlog/spdlog.h>

// 0 = log calls no-ops
// 1 = logging enabled
#define ENABLE_LOGGING 1

/**
 * @brief logging setup and cleanup
 *
 * USAGE:
 *
 * LOG_TRACE    for capturing flow of the program, e.g. to check if function is called or not
 * LOG_DEBUG    info for developers only, e.g. during debugging stages
 * LOG_INFO     all kinds of info, e.g. the used GPU
 * LOG_WARN     used if the error is tolerable
 * LOG_ERROR    used for program errors, e.g. texture loading failed
 * LOG_CRITICAL used for system level errors, e.g. module init failing
 */
namespace Logging {

/**
* @brief initialize logging to console
*/
void init();

void destroy();

}

// basic logger functions with different logging levels
#ifdef ENABLE_LOGGING
#define DEFAULT_LOGGER "MainLog"
#define LOG_TRACE(...)    if (spdlog::get(DEFAULT_LOGGER) != nullptr) {spdlog::get(DEFAULT_LOGGER)->trace(__VA_ARGS__);}
#define LOG_INFO(...)     if (spdlog::get(DEFAULT_LOGGER) != nullptr) {spdlog::get(DEFAULT_LOGGER)->info(__VA_ARGS__);}
#define LOG_DEBUG(...)    if (spdlog::get(DEFAULT_LOGGER) != nullptr) {spdlog::get(DEFAULT_LOGGER)->debug(__VA_ARGS__);}
#define LOG_WARN(...)     if (spdlog::get(DEFAULT_LOGGER) != nullptr) {spdlog::get(DEFAULT_LOGGER)->warn(__VA_ARGS__);}
#define LOG_ERROR(...)    if (spdlog::get(DEFAULT_LOGGER) != nullptr) {spdlog::get(DEFAULT_LOGGER)->error(__VA_ARGS__);}
#define LOG_CRITICAL(...) if (spdlog::get(DEFAULT_LOGGER) != nullptr) {spdlog::get(DEFAULT_LOGGER)->critical(__VA_ARGS__);}
#else
#define LOG_TRACE(...)    (void)0
#define LOG_INFO(...)     (void)0
#define LOG_DEBUG(...)    (void)0
#define LOG_WARN(...)     (void)0
#define LOG_ERROR(...)    (void)0
#define LOG_CRITICAL(...) (void)0
#endif

// compile time log levels
#ifdef ENABLE_LOGGING
#define LOG_TRACE_SRC(...)    SPDLOG_TRACE(__VA_ARGS__)
#define LOG_INFO_SRC(...)     SPDLOG_INFO(__VA_ARGS__)
#define LOG_DEBUG_SRC(...)    SPDLOG_DEBUG(__VA_ARGS__)
#define LOG_WARN_SRC(...)     SPDLOG_WARN(__VA_ARGS__)
#define LOG_ERROR_SRC(...)    SPDLOG_ERROR(__VA_ARGS__)
#define LOG_CRITICAL_SRC(...) SPDLOG_CRITICAL(__VA_ARGS__)
#else
#define LOG_TRACE_SRC(...)    (void)0
#define LOG_INFO_SRC(...)     (void)0
#define LOG_DEBUG_SRC(...)    (void)0
#define LOG_WARN_SRC(...)     (void)0
#define LOG_ERROR_SRC(...)    (void)0
#define LOG_CRITICAL_SRC(...) (void)0
#endif



#endif
