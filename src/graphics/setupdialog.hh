#ifndef SETUP_DIALOG_HH
#define SETUP_DIALOG_HH

#include <QDialog>

class QSpinBox;
class QCheckBox;


class SetupDialog: public QDialog
{
    Q_OBJECT
public:
    SetupDialog();
    ~SetupDialog() = default;

    virtual void accept() override;

signals:
    void boardSizeChosen(unsigned int width, unsigned int height);

private:
    QSpinBox* mSizeBox;

};


#endif
