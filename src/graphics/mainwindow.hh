#ifndef MAIN_WINDOW_HH
#define MAIN_WINDOW_HH

#include <QMainWindow>

class GraphicsScene;
class QGraphicsView;


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow() = default;

    GraphicsScene* getScene();

signals:
    void nextStepRequested();

public slots:
    virtual void keyPressEvent(QKeyEvent *event) override;

private:
    QGraphicsView* mView;
    GraphicsScene* mScene;

};

#endif

