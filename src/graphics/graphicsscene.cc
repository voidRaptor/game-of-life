#include "graphicsscene.hh"

#include <board/cell.hh>
#include <utility/logging.hh>

#include <QBrush>
#include <QGraphicsItem>


void GraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
    if (mouseEvent->button() == Qt::LeftButton)
    {
        auto pos = mouseEvent->scenePos();
        LOG_DEBUG("mouse pos: {}, {}", pos.x(), pos.y());

        auto* item = itemAt(pos, QTransform());
        if (item)
        {
            auto rect = static_cast<QGraphicsRectItem*>(item)->rect();
            auto x = rect.topLeft().x();
            auto y = rect.topLeft().y();

            emit cellClicked(x, y);
        }
    }
}

void GraphicsScene::paintByState(
    const Cell& cell,
    unsigned int cellWidth,
    unsigned int cellHeight
)
{
    auto aliveBrush = QBrush(QColor(10, 10, 10, 255));
    auto deadBrush = QBrush(QColor(255, 255, 255, 255));
    auto pos = QPoint(cell.x * cellWidth, cell.y * cellHeight);
    auto* item = static_cast<QGraphicsRectItem*>(itemAt(pos, QTransform()));

    if (!item)
    {
        LOG_ERROR("Item to be painted not found at index {},{}.", cell.x, cell.y);
        return;
    }

    if (cell.state == State::Alive)
        item->setBrush(aliveBrush);
    else
        item->setBrush(deadBrush);

}

