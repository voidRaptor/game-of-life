#include "graphicsscene.hh"
#include "mainwindow.hh"
#include <utility/logging.hh>
#include <QGraphicsView>
#include <QKeyEvent>


MainWindow::MainWindow():
    mView(new QGraphicsView),
    mScene(new GraphicsScene)
{
    setWindowTitle(tr("Game Of Life"));
    setWindowState(Qt::WindowMaximized);

    mView->setScene(mScene);
    setCentralWidget(mView);

    setFocusPolicy(Qt::WheelFocus);
}

GraphicsScene* MainWindow::getScene()
{
    return mScene;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Space)
        emit nextStepRequested();
    else
        QWidget::keyPressEvent(event);
}

