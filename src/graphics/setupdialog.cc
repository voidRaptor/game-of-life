#include "setupdialog.hh"
#include <QCheckBox>
#include <QFormLayout>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>


SetupDialog::SetupDialog()
{
    setWindowTitle("Choose board size");

    mSizeBox = new QSpinBox;
    mSizeBox->setMinimum(1);
    mSizeBox->setMaximum(1000);
    mSizeBox->setValue(10);

    auto* fieldLayout = new QFormLayout;
    fieldLayout->addRow(new QLabel("Board size in cells"));
    fieldLayout->addRow(mSizeBox, new QLabel(""));

    auto* group = new QGroupBox;
    group->setLayout(fieldLayout);

    auto* cancelButton = new QPushButton("Cancel");
    auto* okButton = new QPushButton("ok");

    auto* buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(cancelButton);
    buttonLayout->addWidget(okButton);

    auto* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(group);
    mainLayout->addLayout(buttonLayout);
    setLayout(mainLayout);

    adjustSize();

    QObject::connect(cancelButton, &QPushButton::pressed, this, &SetupDialog::reject);
    QObject::connect(okButton,     &QPushButton::pressed, this, &SetupDialog::accept);
}

void SetupDialog::accept()
{
    emit boardSizeChosen(mSizeBox->value(), mSizeBox->value());
    QDialog::accept();
}

