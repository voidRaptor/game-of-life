#ifndef GRAPHICS_SCENE_HH
#define GRAPHICS_SCENE_HH

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

class Cell;

class GraphicsScene: public QGraphicsScene
{
    Q_OBJECT
public:
    GraphicsScene() = default;
    ~GraphicsScene() = default;

    virtual void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent) override;
    void paintByState(
        const Cell& cell,
        unsigned int cellWidth,
        unsigned int cellHeight
    );

signals:
    void cellClicked(unsigned int x, unsigned int y);


};



#endif
